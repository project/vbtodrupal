
/**
 * @file
 * Photopost javascript behaviours.
 */

Drupal.behaviors.pptodrupal = function (context) {
  $('.photopost-forum-code').focus(function (e) {
    this.select();
  });

  $('.photopost-forum-code').mouseup(function (e) {
    return false;
  });
}
